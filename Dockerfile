﻿FROM node:14.16-alpine3.13

RUN apk update && apk add libcap && rm -rf /var/cache/apk/* \
    && setcap cap_net_bind_service=+ep '/usr/local/bin/node' \
    && mkdir -p /meshcentral \
    && mkdir -p /meshcentral/node_modules \
    && mkdir -p /meshcentral/meshcentral-data \
    && mkdir -p /meshcentral/meshcentral-files \
    && chown -R node:node /meshcentral

WORKDIR /meshcentral

COPY package.json package.json
COPY package-lock.json package-lock.json

ARG NODE_ENV="production"
ARG MESHCENTRAL2_VERSION="0.6.70"

RUN npm install --save meshcentral@${MESHCENTRAL2_VERSION}

EXPOSE 80 443 4433 

VOLUME [ "/meshcentral/meshcentral-data", "/meshcentral/meshcentral-files" ]

CMD [ "node", "./node_modules/meshcentral" ]
